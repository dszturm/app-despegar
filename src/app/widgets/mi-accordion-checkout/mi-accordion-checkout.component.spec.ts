import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MiAccordionCheckoutComponent } from './mi-accordion-checkout.component';

describe('MiAccordionCheckoutComponent', () => {
  let component: MiAccordionCheckoutComponent;
  let fixture: ComponentFixture<MiAccordionCheckoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MiAccordionCheckoutComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MiAccordionCheckoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
