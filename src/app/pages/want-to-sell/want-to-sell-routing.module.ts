import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WantToSellPage } from './want-to-sell.page';

const routes: Routes = [
  {
    path: '',
    component: WantToSellPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WantToSellPageRoutingModule {}
