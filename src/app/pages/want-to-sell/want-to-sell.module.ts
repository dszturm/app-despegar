import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WantToSellPageRoutingModule } from './want-to-sell-routing.module';

import { WantToSellPage } from './want-to-sell.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WantToSellPageRoutingModule
  ],
  declarations: [WantToSellPage]
})
export class WantToSellPageModule {}
