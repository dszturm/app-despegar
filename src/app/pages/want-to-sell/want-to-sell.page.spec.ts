import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WantToSellPage } from './want-to-sell.page';

describe('WantToSellPage', () => {
  let component: WantToSellPage;
  let fixture: ComponentFixture<WantToSellPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WantToSellPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WantToSellPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
