import { PagesService } from './../../services/pages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-exchange-policy',
  templateUrl: './exchange-policy.page.html',
  styleUrls: ['./exchange-policy.page.scss'],
})
export class ExchangePolicyPage implements OnInit {

  response: any;
  page: any;
  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.pageService.getPageBySlug('politica-de-troca-e-devolucao').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
      }
    });
  }
}
