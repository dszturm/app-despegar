import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ExchangePolicyPageRoutingModule } from './exchange-policy-routing.module';

import { ExchangePolicyPage } from './exchange-policy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ExchangePolicyPageRoutingModule
  ],
  declarations: [ExchangePolicyPage]
})
export class ExchangePolicyPageModule {}
