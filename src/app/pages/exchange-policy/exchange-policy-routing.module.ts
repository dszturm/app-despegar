import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ExchangePolicyPage } from './exchange-policy.page';

const routes: Routes = [
  {
    path: '',
    component: ExchangePolicyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExchangePolicyPageRoutingModule {}
