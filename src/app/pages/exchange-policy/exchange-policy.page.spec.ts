import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ExchangePolicyPage } from './exchange-policy.page';

describe('ExchangePolicyPage', () => {
  let component: ExchangePolicyPage;
  let fixture: ComponentFixture<ExchangePolicyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangePolicyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ExchangePolicyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
