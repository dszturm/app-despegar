import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.page.html',
  styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit { 
  yudsegment: string;

  /**
 * The data structure that will be used for supplying the accordion content
 * @public
 * @property technologies
 * @type {Array}
 */
public technologies : Array<{ name: string, description: string, image: string }> = [
  { 
     name : 'Itens', 
     image: '/assets/img/img-perfil.png',
     description : 'Bolsa Fendi 2Jours Medium Marrom',
    
  }
];
  constructor() { }

  ionViewWillEnter(){
    this.yudsegment = "today";
  }
  ngOnInit() {
  }
/**
   * Captures and console logs the value emitted from the user depressing the accordion component's <ion-button> element 
   * @public
   * @method captureName
   * @param {any}		event 				The captured event
   * @returns {none}
   */
  public captureName(event: any) : void
  {
     console.log(`Captured name by event value: ${event}`);
  }
}
