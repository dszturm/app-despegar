import { PagesService } from './../../services/pages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {
  response: any;
  page:any;

  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.pageService.getPageBySlug('politica-de-privacidade').subscribe(
      data => {
        console.log(data);
        this.response = data;
        if(this.response.page){
          this.page = this.response.page;
          console.log(this.page)
        }
    });
  }
  
}
