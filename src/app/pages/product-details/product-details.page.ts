import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/interfaces/product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.page.html',
  styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage {

  products: Array<Product>;
  categoryID: string;
  type: string;
  response: any;

  constructor(private productService: ProductService) { }

  ionViewWillEnter() {

    // this.categoryID = this.activatedRoute.snapshot.paramMap.get('catID');
    this.productService.getProductsById(1).subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.products) {
        this.products = this.response.products;
      }
    });
    this.type = 'info';
  }

}
