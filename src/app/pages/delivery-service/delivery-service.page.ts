import { PagesService } from './../../services/pages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-delivery-service',
  templateUrl: './delivery-service.page.html',
  styleUrls: ['./delivery-service.page.scss'],
})
export class DeliveryServicePage implements OnInit {

  response: any;
  page: any;
  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.pageService.getPageBySlug('servico-de-entrega').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
      }
    });
  }
}
