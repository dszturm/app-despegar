import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DeliveryTimePageRoutingModule } from './delivery-time-routing.module';

import { DeliveryTimePage } from './delivery-time.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeliveryTimePageRoutingModule
  ],
  declarations: [DeliveryTimePage]
})
export class DeliveryTimePageModule {}
