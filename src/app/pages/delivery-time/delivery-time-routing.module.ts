import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveryTimePage } from './delivery-time.page';

const routes: Routes = [
  {
    path: '',
    component: DeliveryTimePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeliveryTimePageRoutingModule {}
