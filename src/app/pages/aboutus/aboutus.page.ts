import { Component, OnInit } from '@angular/core';
import { PagesService } from 'src/app/services/pages.service';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.page.html',
  styleUrls: ['./aboutus.page.scss'],
})
export class AboutusPage implements OnInit {
  response: any;
  page: any;
  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.pageService.getPageBySlug('quem-somos').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
      }
    });
  }
}
