import { PagesService } from './../../services/pages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authenticity',
  templateUrl: './authenticity.page.html',
  styleUrls: ['./authenticity.page.scss'],
})
export class AuthenticityPage implements OnInit {

  response: any;
  page: any;
  title:any;
  tag:any;
  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.pageService.getPageBySlug('autenticidade').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
        this.title = this.response.titulos;
        this.tag = this.response.tags;
      }
    });
  }
}
