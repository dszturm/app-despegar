import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AuthenticityPageRoutingModule } from './authenticity-routing.module';

import { AuthenticityPage } from './authenticity.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AuthenticityPageRoutingModule
  ],
  declarations: [AuthenticityPage]
})
export class AuthenticityPageModule {}
