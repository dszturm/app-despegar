import { PagesService } from './../../services/pages.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage implements OnInit {

  response: any;
  page: any;
  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.pageService.getPageBySlug('termos-de-uso').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
      }
    });
  }
}
