import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderCancellationPageRoutingModule } from './order-cancellation-routing.module';

import { OrderCancellationPage } from './order-cancellation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderCancellationPageRoutingModule
  ],
  declarations: [OrderCancellationPage]
})
export class OrderCancellationPageModule {}
