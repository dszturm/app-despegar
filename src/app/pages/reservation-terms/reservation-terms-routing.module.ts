import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReservationTermsPage } from './reservation-terms.page';

const routes: Routes = [
  {
    path: '',
    component: ReservationTermsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReservationTermsPageRoutingModule {}
