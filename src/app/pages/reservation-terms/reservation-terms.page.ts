import { Component, OnInit } from '@angular/core';
import { PagesService } from 'src/app/services/pages.service';


@Component({
  selector: 'app-reservation-terms',
  templateUrl: './reservation-terms.page.html',
  styleUrls: ['./reservation-terms.page.scss'],
})
export class ReservationTermsPage implements OnInit {

  response: any;
  page: any;
  constructor(private pageService: PagesService) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.pageService.getPageBySlug('termos-e-condicoes-do-rd-reserva-de-luxo').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
      }
    });
  }
  }


