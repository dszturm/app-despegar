import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReservationTermsPageRoutingModule } from './reservation-terms-routing.module';

import { ReservationTermsPage } from './reservation-terms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReservationTermsPageRoutingModule
  ],
  declarations: [ReservationTermsPage]
})
export class ReservationTermsPageModule {}
