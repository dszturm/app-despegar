import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HowToBuyPage } from './how-to-buy.page';

describe('HowToBuyPage', () => {
  let component: HowToBuyPage;
  let fixture: ComponentFixture<HowToBuyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowToBuyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HowToBuyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
