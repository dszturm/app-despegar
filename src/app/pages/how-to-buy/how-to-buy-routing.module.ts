import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HowToBuyPage } from './how-to-buy.page';

const routes: Routes = [
  {
    path: '',
    component: HowToBuyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HowToBuyPageRoutingModule {}
