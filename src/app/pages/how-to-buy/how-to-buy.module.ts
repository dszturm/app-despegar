import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HowToBuyPageRoutingModule } from './how-to-buy-routing.module';

import { HowToBuyPage } from './how-to-buy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HowToBuyPageRoutingModule
  ],
  declarations: [HowToBuyPage]
})
export class HowToBuyPageModule {}
