import { Component, OnInit } from '@angular/core';
import { PagesService } from './../../services/pages.service';


@Component({
  selector: 'app-how-to-buy',
  templateUrl: './how-to-buy.page.html',
  styleUrls: ['./how-to-buy.page.scss'],
})
export class HowToBuyPage implements OnInit {

  response: any;
  page: any;
  constructor(private pageService: PagesService) { }


  ngOnInit() {
  }

  ionViewWillEnter() {
    this.pageService.getPageBySlug('como-comprar').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.conteudo) {
        this.page = this.response.conteudo;
      }
    });
  }
}
