import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modalone',
  templateUrl: './modalone.page.html',
  styleUrls: ['./modalone.page.scss'],
})
export class ModalonePage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }
  async showModalOne() {
    const modal = await this.modalCtrl.create({
      component: ModalonePage,
      cssClass: 'my-custom-modal-css'
    });
    return await modal.present();
  }
  close() {
    this.modalCtrl.dismiss();
  }
}
