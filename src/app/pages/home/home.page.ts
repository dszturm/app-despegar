import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(public navCtrl: NavController, private menu: MenuController) { }

  ngOnInit() {
  }

  goToProductFeed(){
    this.navCtrl.navigateForward('products-feed');
  }

  goToProductDetails(){
    this.navCtrl.navigateForward('tabs/product-details')
  }

  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
}
