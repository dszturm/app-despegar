import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NotFindPage } from './not-find.page';

describe('NotFindPage', () => {
  let component: NotFindPage;
  let fixture: ComponentFixture<NotFindPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFindPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NotFindPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
