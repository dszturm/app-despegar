import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NotFindPageRoutingModule } from './not-find-routing.module';

import { NotFindPage } from './not-find.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NotFindPageRoutingModule
  ],
  declarations: [NotFindPage]
})
export class NotFindPageModule {}
