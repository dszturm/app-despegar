import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NotFindPage } from './not-find.page';

const routes: Routes = [
  {
    path: '',
    component: NotFindPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NotFindPageRoutingModule {}
