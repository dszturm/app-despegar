import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { CartPageRoutingModule } from './cart-routing.module';

import { CartPage } from './cart.page';
import { MiAccordionComponent } from '../../widgets/mi-accordion/mi-accordion.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartPageRoutingModule,
    RouterModule.forChild([
      {
        path: '',
        component: CartPage
      }
    ])
  ],
  declarations: [CartPage, MiAccordionComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]


})
export class CartPageModule {}
