import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalonePage } from '../modalone/modalone.page';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }
async showModalOne(){
  const modal = await this.modalCtrl.create({
    component: ModalonePage,
    cssClass: 'my-custom-modal-css'
  });
  modal.present();
}
 
}
