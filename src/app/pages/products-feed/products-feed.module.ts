import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsFeedPageRoutingModule } from './products-feed-routing.module';

import { ProductsFeedPage } from './products-feed.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsFeedPageRoutingModule
  ],
  declarations: [ProductsFeedPage]
})
export class ProductsFeedPageModule {}
