import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsFeedPage } from './products-feed.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsFeedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsFeedPageRoutingModule {}
