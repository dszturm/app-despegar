import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductsFeedPage } from './products-feed.page';

describe('ProductsFeedPage', () => {
  let component: ProductsFeedPage;
  let fixture: ComponentFixture<ProductsFeedPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsFeedPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsFeedPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
