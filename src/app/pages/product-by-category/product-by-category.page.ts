import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../services/product.service';
import { Product } from '../../interfaces/product';

@Component({
  selector: 'app-product-by-category',
  templateUrl: './product-by-category.page.html',
  styleUrls: ['./product-by-category.page.scss'],
})
export class ProductByCategoryPage {
  products: Array<Product>;
  categoryID: string;
  response: any;

  constructor(private productService: ProductService) { }


  ionViewWillEnter() {
    // this.categoryID = this.activatedRoute.snapshot.paramMap.get('catID');
    this.productService.getProductsByCategory('bolsas').subscribe(data => {
      console.log(data);
      this.response = data;
      if (this.response.products) {
        this.products = this.response.products;
      }

    });
  }

}
