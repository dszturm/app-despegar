import { Component } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-productform',
  templateUrl: './productform.page.html',
  styleUrls: ['./productform.page.scss'],
})
export class ProductformPage {

  type: any;
  images: any = [];
  constructor(public navCtrl: NavController, public imagePicker: ImagePicker, public file: File) { }

  ionViewWillEnter() {
    this.type = 'termos';
  }

  PickMultipleImages() {
    var options: ImagePickerOptions = {
      maximumImagesCount: 5,
      width: 100,
      height: 100
    }
    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);

        let filename = results[i].substring(results[i]
          .lastIndexOf('/') + 1);

        let path = results[i].substring(0, results[i]
          .lastIndexOf('/') + 1);

        this.file.readAsDataURL(path, filename).then((base64string) => {
          this.images.push(base64string);
        })
      }
    }, (err) => { });
  }

  NextPage() {
    switch (this.type){
      case 'termos':
        this.type = 'dados';
        break;
      case 'dados':
        this.type = 'condicoes';
        break;
      case 'condicoes':
        
    }
  }
}
