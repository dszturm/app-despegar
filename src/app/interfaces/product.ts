export interface Product {


    'id': number,
    'code'?: string,
    'title'?: string,
    'slug': string,
    'description'?: string,
    'weight'?: string,
    'status'?: number,
    'new'?: boolean,
    'sale'?: boolean,
    'free_shipping'?: boolean,
    'installments'?: number,
    'cost': string,
    'price': string,
    'created_at'?: string,
    'updated_at'?: string,
    'categorie'?: {
        'id': string,
        'title': string,
        'slug'?: string
    },
    'sizes'?: {
        'id': boolean,
        'name': string
    },
    'brand': {
        'id': string
        'title': string
    },
    'package'?: {
        'length': string,
        'width': string,
        'height': string,
        'weight': string
    },
    'images'?: any



}