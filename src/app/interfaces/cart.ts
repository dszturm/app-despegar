import { Product } from './product';

export interface Cart {
   products: Array<CartProduct>
   total?:0.00
}
export interface CartProduct {
   product:Product,
   qty:number,
   productTotal:number,
   unitPrice:number
}
