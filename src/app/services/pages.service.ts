import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PagesService {

  url = "https://apiteste.resolvidesapegar.com.br/page/index/"
  constructor(private http: HttpClient) { }

  getPageBySlug(slug) {

    const response = this.http.get(`${this.url}${slug}.html`);
    return response;
  }


}
