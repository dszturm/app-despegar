import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

export interface Product {
  id: number;
  name: string;
  price: number;
  amount: number;
}

@Injectable({
  providedIn: 'root'
})
export class CartService {
  data: Product[] = [
    { id: 0, name: 'Product Test', price: 5.50, amount: 1 },
    { id: 1, name: 'Product Test', price: 9.63, amount: 1 },
    { id: 2, name: 'Product Test', price: 42.96, amount: 1 },
    { id: 3, name: 'Product Test', price: 15.74, amount: 1 },
  ];

  private cart = [];
  private cartItemCount = new BehaviorSubject(0);
  constructor() { }
}
