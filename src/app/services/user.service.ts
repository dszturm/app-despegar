import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  signin(){
    
  }
  signUp(user){
    const formData = new FormData();
    formData.append('nome', user.name);
    formData.append('cpf', user.cpf);
    formData.append('telefone', user.brand);
    formData.append('email', user.size);
    formData.append('data_nascimento', user.size);
    formData.append('pesquisa', user.size);
    formData.append('senha', user.size);
    formData.append('conf_senha', user.size);
    formData.append('logradouro', user.size);
    formData.append('numero', user.size);
    formData.append('complemento', user.size);
    formData.append('cidade', user.size);
    formData.append('cep', user.size);
    formData.append('bairro', user.size);
    formData.append('uf', user.size);
    const response = this.http.post("https://apiteste.resolvidesapegar.com.br/users/create/", formData)
    return response;
    
  }
  signout(){
    
  }
  updateUser(user){

    const formData = new FormData();
    formData.append('nome', user.name);
    formData.append('cpf', user.cpf);
    formData.append('telefone', user.brand);
    formData.append('email', user.size);
    formData.append('data_nascimento', user.size);
    formData.append('pesquisa', user.size);
    formData.append('senha', user.size);
    formData.append('conf_senha', user.size);
    formData.append('logradouro', user.size);
    formData.append('numero', user.size);
    formData.append('complemento', user.size);
    formData.append('cidade', user.size);
    formData.append('cep', user.size);
    formData.append('bairro', user.size);
    formData.append('uf', user.size);
    const response = this.http.post("https://apiteste.resolvidesapegar.com.br/users/update/", formData)
    return response;
  }
}
