import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url = "https://apiteste.resolvidesapegar.com.br/"
  constructor(private http: HttpClient) { }

  getProductsByCategory(categoryId) {
    const formData = new FormData();
    formData.append('category', categoryId);
    const response = this.http.post<Product>("https://apiteste.resolvidesapegar.com.br/products", formData)
    return response;

  }
  getProductsById(productId) {
    const formData = new FormData();
    formData.append('id', productId);
    const response = this.http.post<Product>("https://apiteste.resolvidesapegar.com.br/products", formData)
    return response;
  }
  searchProducts(search) {
    const formData = new FormData();
    formData.append('id', search.productId);
    formData.append('categorie', search.category);
    formData.append('brand', search.brand);
    formData.append('size', search.size);
    const response = this.http.post<Product>("https://apiteste.resolvidesapegar.com.br/products", formData)
    return response;
  }
  getProduct(id,slug){
    const formData = new FormData();
    formData.append('id', id);
    formData.append('slug', slug);
    const response = this.http.post<Product>("https://apiteste.resolvidesapegar.com.br/product", formData)
    return response;

  }
  getCategories() {
    const response = this.http.get("https://apiteste.resolvidesapegar.com.br/categories")
    return response;
  }
  getBrands() {
    const response = this.http.get("https://apiteste.resolvidesapegar.com.br/brands")
    return response;
  }

}
