import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'productform',
    loadChildren: () => import('./pages/productform/productform.module').then( m => m.ProductformPageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./pages/search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./pages/account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'galery',
    loadChildren: () => import('./pages/galery/galery.module').then( m => m.GaleryPageModule)
  },
  {
    path: 'products-feed',
    loadChildren: () => import('./pages/products-feed/products-feed.module').then( m => m.ProductsFeedPageModule)
  },
  {
    path: 'product-by-category',
    loadChildren: () => import('./pages/product-by-category/product-by-category.module').then( m => m.ProductByCategoryPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./pages/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'modalone',
    loadChildren: () => import('./pages/modalone/modalone.module').then( m => m.ModalonePageModule)
  },
  {
    path: 'product-details',
    loadChildren: () => import('./pages/product-details/product-details.module').then( m => m.ProductDetailsPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./pages/checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'aboutus',
    loadChildren: () => import('./pages/aboutus/aboutus.module').then( m => m.AboutusPageModule)
  },
  {
    path: 'privacy',
    loadChildren: () => import('./pages/privacy/privacy.module').then( m => m.PrivacyPageModule)
  },
  {
    path: 'terms',
    loadChildren: () => import('./pages/terms/terms.module').then( m => m.TermsPageModule)
  },
  {
    path: 'exchange-policy',
    loadChildren: () => import('./pages/exchange-policy/exchange-policy.module').then( m => m.ExchangePolicyPageModule)
  },
  {
    path: 'how-to-buy',
    loadChildren: () => import('./pages/how-to-buy/how-to-buy.module').then( m => m.HowToBuyPageModule)
  },
  {
    path: 'authenticity',
    loadChildren: () => import('./pages/authenticity/authenticity.module').then( m => m.AuthenticityPageModule)
  },
  {
    path: 'delivery-service',
    loadChildren: () => import('./pages/delivery-service/delivery-service.module').then( m => m.DeliveryServicePageModule)
  },
  {
    path: 'not-find',
    loadChildren: () => import('./pages/not-find/not-find.module').then( m => m.NotFindPageModule)
  },
  {
    path: 'delivery-time',
    loadChildren: () => import('./pages/delivery-time/delivery-time.module').then( m => m.DeliveryTimePageModule)
  },  {
    path: 'order-cancellation',
    loadChildren: () => import('./pages/order-cancellation/order-cancellation.module').then( m => m.OrderCancellationPageModule)
  },
  {
    path: 'want-to-sell',
    loadChildren: () => import('./pages/want-to-sell/want-to-sell.module').then( m => m.WantToSellPageModule)
  },
  {
    path: 'reservation',
    loadChildren: () => import('./pages/reservation/reservation.module').then( m => m.ReservationPageModule)
  },
  {
    path: 'reservation-terms',
    loadChildren: () => import('./pages/reservation-terms/reservation-terms.module').then( m => m.ReservationTermsPageModule)
  },


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
